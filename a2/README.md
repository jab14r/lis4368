> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Joseph DiMartino Brierton

### Assignment 2 Requirements:

1. Assessment Links
2. Screenshot of query results


#### README.md file should include the following items:

1. Assessment Links
2. Screenshot of query results

> This is a blockquote.
>
> This is the second paragraph in the blockquote.


##### Assignment Screenshots:

*Screenshot of query results from querybook.html

![query results screenshot](img/querybook.png)


#### Assessment Links:

*Link: localhost:999/hello*
[Link: localhost:999/hello](http://localhost:9999/hello)

*Link: HelloHome, index.html*
[http://localhost:9999/hello/index.html](http://localhost:9999/hello/index.html)

*Link: Invoking HelloServlet*
[Link: http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello)

*Link: querybook*
[Link: http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)

*Link: AnotherHelloServlet*
[Link: http://localhost:9999/hello/sayhi](http://localhost:9999/hello/sayhi)
