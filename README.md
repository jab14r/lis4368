> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4368

## Joseph DiMartino Brierton

### Assignment # Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install VCS
	- Push to Git
	

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Install MySql
	- Develop and Deploy WebApp
	- Debug


3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Create MySql Database/Tables
	- Forward Engineer Tables
	- Push to Git
	
3. [A4 README.md](a4/README.md "My A4 README.md file")
	- Set up model view controller
	- Edit and comple customer.java and customerlistservlet.java
	- Screenshot passed and failed validations
	
4. [A5 README.md](a5/README.md "My A5 README.md file")
	- Screenshot of valid user form entry
	- Screenshot of passed validation
	- Screenshot of associated database entry

5. [P1 README.md](p1/README.md "My P1 README.md file")
	- Clone assignment starter files
	- Review subdirectories and files
	- Open index.jsp and review code
	
5. [P2 README.md](p2/README.md "My P2 README.md file")
	- Review and modify jsps and servlet files; compile
	- Create modify.jsp; compile
	- Screenshot different validations