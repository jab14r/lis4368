> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Joseph DiMartino Brierton

### Assignment 4 Requirements:

1. Set up model view controller
2. Compile customer.java and customerlistservlet.java
3. Screenshot different validations


#### README.md file should include the following items:

* Screenshot of failed validation
* Screenshot of passed validation


> This is a blockquote.
>
> This is the second paragraph in the blockquote.


##### Assignment Screenshots:


* Screenshot of failed validation;

![ERD Screenshot](img/failed.png)

* Screenshot of passed validation;

![ERD Screenshot](img/passed.png)
