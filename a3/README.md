> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Joseph DiMartino Brierton

### Assignment 3 Requirements:

1. Build ERDs/Database in MySQL Workbench
2. Forward engineer tables.
3. Push to Git.


#### README.md file should include the following items:

1. Links to .mwb and .sql
2. Screenshot of ERDs.


##### Assignment Links:

*Links to Files

* Link to .mwb [assignment3.mwb](assigment33.mwb)
*Link to .sql [a3.sql](a3.sql)


#### ERD Screenshot:

![query results screenshot](img/erds.png)

