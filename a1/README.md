> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4368

## Joseph DiMartino Brierton

### Assignment # Requirements:

*3 Parts:*

1. Distribute Version Control and Dev Environments
2. Development Installation
3. MC

#### README.md file should include the following items:

* Screentshot of java Hello
* Screenshort of running http://localhost:9999
* git commands w/ short descriptions
* Bitbucket repo links

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - creates empty git repository
2. git status - states status of current git repo
3. git add - add files
4. git commit - commit changes
5. git push - pushes local files to remote repository
6. git pull - pulls files to local repo from remote repo
7. git merge - merges repos

#### Assignment Screenshots:

*Screenshot of Tomcat running http://localhost*:

![Tomcat Installation Screenshot](img/tomcat.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/hellojava.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jab14r/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/jab14r/myteamquotes/ "My Team Quotes Tutorial")
