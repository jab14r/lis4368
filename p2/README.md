> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Joseph DiMartino Brierton

### Project 2 Requirements:

1. Review and modify jsps and servlet files; compile
2. Create modify.jsp; compile
3. Screenshot different validations


#### README.md file should include the following items:

* Screenshot of valid user form entry
* Screenshot of passed validation
* Screenshot of displayed data
* Screenshot of Modify Form
* Screenshot of modified data
* Screenshot of delete warning
* Screenshot of associated database entry

> This is a blockquote.
>
> This is the second paragraph in the blockquote.


##### Assignment Screenshots:


* Screenshot of valid user form entry;

![Entered Information](img/validformentry.png)

* Screenshot of passed validation;

![Passed Validation](img/passedvalidation.png)

* Screenshot of displayed data;

![Displayed data](img/displaydata.png)

* Screenshot of modify form;

![Passed Validation](img/modifyform.png)

* Screenshot of modified data;

![Passed Validation](img/modified.png)

* Screenshot of delete warning;

![Passed Validation](img/deletenot.png)

* Screenshot of associated database entry;

![Associated Database Entry 1](img/2tables1.png)
![Associated Database Entry 2](img/2tables2.png)
