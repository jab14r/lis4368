# LIS 4368 - Web Applications Development

## Joseph DiMartino Brierton

### Project 1 Requirements:

1. Clone assignment starter files
2. Review subdirectories and files
3. Open index.jsp and review code

#### README.md file should include the following items:

* Screenshot of main page;
* Screenshot of failed validations
* Screenshot of passed validation



##### Assignment Screenshots:

* Screenshot of main page:

![ERD Screenshot](img/home.png)

* Screenshot of failed validations:

![Failed Validation Screenshot 1](img/fieldsnotworking.png)

![Failed Validation Screenshot 2](img/fieldsnotworking2.png)

* Screenshot of passed validation:

![ERD Screenshot](img/fieldsworking.png)
