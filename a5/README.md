> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Joseph DiMartino Brierton

### Assignment 5 Requirements:

1. Review and modify thanks.jsp and CustomerList Servlet.java; compile
2. Create ConnectionPool.java, CustomerDB.java and DBUtil.java; compile
3. Screenshot different validations


#### README.md file should include the following items:

* Screenshot of valid user form entry
* Screenshot of passed validation
* Screenshot of associated database entry

> This is a blockquote.
>
> This is the second paragraph in the blockquote.


##### Assignment Screenshots:


* Screenshot of valid user form entry;

![Entered Information](img/enteredinfo.png)

* Screenshot of passed validation;

![Passed Validation](img/thanks.png)

* Screenshot of associated database entry;

![Associated Database Entry](img/newrecord.png)
